local params = {...}
local cursor = 1
local preCursor = -1
local selectedColor = colors.yellow
local titleColor = colors.white
local titleBgColor = colors.gray
local bgColor = colors.cyan
local txtColor = colors.white
local timerCount = nil
local menuExtraText = ""
local selectedIndicatorEnabled = true
version = {1, 2, 0}
versionString = "v"..tostring(version[1]).."."..tostring(version[2]).."."..tostring(version[3])

local function centertext(text, y)
  local w, h = term.getSize()
  term.setCursorPos((w/2)-(#text /2), y)
  term.write(text)
 end

function draw(title, menu, extra, scroll, sel, hideSelectedNum)
  local w, h = term.getSize()
  term.setBackgroundColor(bgColor)
  term.clear()
  local t1 = ""
  if type(extra) == "number" or type(extra) == "string" then
    t1 = tostring(extra)
  else
    t1 = ""
  end
  for i=1, #menu do
    if(i == (sel or 0)) then
      term.setTextColor(selectedColor)
      if not false then
      if not term.isColor() then
        centertext("["..menu[i].."]", (h/2)+i-((scroll or 1)-1))
      else
        centertext(" "..menu[i].." ", (h/2)+i-((scroll or 1)-1))
      end
      else
        p = 8 + ((i-(curCursor-1))/5)
        term.setCursorPos(p + 4, (h/2)+i-((scroll or 1)-1))
        term.write(" "..menu[i].." ")
      end
    else
      if not false then
        term.setTextColor(txtColor)
        centertext(" "..menu[i].." ", (h/2)+i-((scroll or 1)-1))
      else
        p = 8 + ((i-((scroll or 1)-1))/5)
        term.setCursorPos(p + math.abs(2 ^ math.abs((scroll or 1)-i) / 40), (h/2)+i-((scroll or 1)-1))
        term.write(menu[i])
      end
    end   
  end
  term.setCursorPos((w/2)-(#title/2)-4,2)
  term.setBackgroundColor(titleBgColor)
  term.setTextColor(titleColor)
  write((" "):rep(#title+8))
  centertext(title, 2)
  term.setCursorPos(1, 1)
  term.clearLine()
  if not hideSelectedNum then
    write(" " .. cursor .. "/" .. #menu .. " ") 
  end
  term.setCursorPos(w - 1 - t1:len(), 1)
  term.write(t1)
  term.setCursorPos((w/2)-(#title/2)-4,2)
  term.setBackgroundColor(titleBgColor)
  term.setTextColor(bgColor)
  term.write(string.char(144))
  term.setCursorPos((w/2)+(#title/2)+4,2)
  term.setBackgroundColor(bgColor)
  term.setTextColor(titleBgColor)
  term.write(string.char(159))
end

function setColors(title, titleBg, bg, selTxt, txt)
    if txt == nil or title == nil or titleBg == nil or selTxt == nil or bg == nil then error("Specify 5 colors: Title text, Title background, Selection background, Selection text, and Regular Selection text", 2) end
    titleColor, titleBgColor, bgColor, selectedColor, txtColor = title, titleBg, bg, selTxt, txt
end

function resetColors()
  selectedColor = colors.white
  titleColor = colors.white
  titleBgColor = colors.gray
  bgColor = colors.cyan
  txtColor = colors.white
end

function setTimer(number, not_suppored) error("function no longer supported at this time. refer to the \'lua\" program on the shell for all available functions.") end

function setCursorPos(n)
  if type(n) == "number" then preCursor = (n < 1 and 1 or n) else preCursor = -1 end
end

function getCursorPos()
  return preCursor
end

function setExtraText(text)
  if pcall(tostring, text) then
	menuExtraText = tostring(text)
  else
	menuExtraText = ""
  end
end

function getExtraText()
  return menuExtraText
end

function enableSelIndicator()
  selectedIndicatorEnabled = true
end

function disableSelIndicator()
  selectedIndicatorEnabled = false
end

function resetSelIndicatorEnabled()
  selectedIndicatorEnabled = true
end

function getSelIndicatorEnabled()
  return selectedIndicatorEnabled
end

local cursorHighlight
function setHighlight(n) -- selects to the choice you want to highlight, resets after finish selecting at start of menu draw
if type(n) ~= "number" then error("expected a number to highlight") end
cursorHighlight = n
end

function menu(title, ...)
	local args = {...}
	local options
	cursor = (type(cursorHighlight) == "number" and cursorHighlight or (preCursor > 0 and preCursor or 1))
	if type(args[1]) == "table" then
		options = args[1]
	else
		options = {...}
	end
	count = timerCount
	countDownTimer = nil
	if count then
		countDownTimer = os.startTimer(1)
	end
	if cursorHighlight then
		dist = n-cursor
		for i = 1,20 do
			cursor = cursor + (dist/20)
			draw(title, options, menuExtraText, cursor, nil, not selectedIndicatorEnabled)
			sleep(0.05)
		end
		for i = 1, 2 do
			draw(title, options, menuExtraText, cursor, cursor, not selectedIndicatorEnabled)
			sleep(0.4)
			draw(title, options, menuExtraText, cursor, nil, not selectedIndicatorEnabled)
			sleep(0.4)
		end
		draw(title, options, menuExtraText, cursor, cursor, not selectedIndicatorEnabled)
  end

  while true do
    draw(title, options, menuExtraText, cursor, cursor, not selectedIndicatorEnabled)
    event = {os.pullEvent()}
    if event[1] == "mouse_click" then
    if event[2] == 1 then
      break
    elseif event[2] == 2 then
      cursor = -cursor
      break
    end
    elseif event[1] == "mouse_scroll" then
    cursor = cursor + event[2]
    elseif event[1] == "key" then
    if(event[2] == keys.up) or (event[2] == keys.w) then
      cursor = cursor-1
    end
    if(event[2] == keys.down) or (event[2] == keys.s) then
      cursor = cursor+1
    end
    if(event[2] == keys.backspace) then
      --for i = 1, 10 do
      --  selectedColor = tmp
      --  draw(title, options, menuExtraText, cursor, cursor)
      --  sleep(0.05)
      --  selectedColor = tmp2
      --  draw(title, options, menuExtraText, cursor, cursor)
      --  sleep(0.05)
      --end
      --selectedColor = tmp
      --draw(title, options, menuExtraText, cursor, cursor)
      term.setBackgroundColor(colors.black)
      term.setTextColor(colors.white)
      term.clear()
      term.setCursorPos(1, 1)
    cursor = -cursor
      break
    end
    if(event[2] == keys.enter) then
      --for i = 1, 10 do
      --  selectedColor = tmp
      --  draw(title, options, menuExtraText, cursor, cursor)
      --  sleep(0.05)
      --  selectedColor = tmp2
      --  draw(title, options, menuExtraText, cursor, cursor)
      --  sleep(0.05)
      --end
      --selectedColor = tmp
      --draw(title, options, menuExtraText, cursor, cursor)
      term.setBackgroundColor(colors.black)
      term.setTextColor(colors.white)
      term.clear()
      term.setCursorPos(1, 1)
      break
    end
    end
    if event[1] == "timer" and event[2] == countDownTimer then 
      count = count - 1
      if count < 0 then
        break
      end
    end
    if(cursor > #options) then cursor = #options end
    if(cursor < 1) then cursor = 1 end
  end

  if count then
    os.cancelTimer(countDownTimer)
  end

  local final_cur = cursor
  cursor = 1
  return final_cur
end

function menuRaw(title, ...)
  local res = menu(title, ...)
  local opt = {...}
  local resopt = ""
  if type(opt[1]) == "table" then
    resopt = opt[1]
  else
    resopt = opt
  end
  return {title, resopt, menuExtraText, res, res, not selectedIndicatorEnabled}
end

function formatRMR(result)
  if not type(result) == "table" then error("Error parsing raw menu result: Expected a table", 2) end; if not #result == 5 then
      error("Error parsing raw menu result: Expected a valid result table", 2) end; if not (type(result[1]) == "string" or type(result[1]) == "number") then
      error("Error parsing raw menu result: Expected a valid result table", 2) end; if not type(result[2]) == "table" then
      error("Error parsing raw menu result: Expected a valid result table", 2) end; if not (type(result[3]) == "string" or type(result[3]) == "number") then
      error("Error parsing raw menu result: Expected a valid result table", 2) end; if not type(result[4]) == "number" then
      error("Error parsing raw menu result: Expected a valid result table", 2) end; if not type(result[5]) == "number" then
      error("Error parsing raw menu result: Expected a valid result table", 2) end; if not type(result[6]) == "boolean" then
      error("Error parsing raw menu result: Expected a valid result table", 2) end;
      return table.unpack(result)
end